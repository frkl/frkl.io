+++
title = "About"
date = "2017-06-27T17:39:21-07:00"
draft = false
+++

**frkl** is my small software and consulting enterprise.

It's an experiment. I'm trying to figure out whether it's possible to work on the (slightly weird) software I want to exist in the world and also earn a living.
