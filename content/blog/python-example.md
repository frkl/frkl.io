+++
title = "Python example"
date = "2019-08-01T00:00:00+02:00"
tags = ["freckles"]
categories = ["frecklet", "python"]
draft = true
+++

We'll be writing a relatively simple
[*frecklet*](https://freckles.io/doc/frecklets), not different at all
to anything we'd write for a different target type (e.g. a remote
server). For more details on how to all this works, and how you can
write your own, please check the [*freckles* getting started
guide](https://freckles.io/doc/getting_started) and the [other
frecklet-related documentation](https://freckles.io/doc/frecklets).

For our example, let's do something 'a bit but not too' involved:
setting up the [RealWorld example Python flask
app](https://github.com/gothinkster/flask-realworld-example-app) (with
systemd service unit), including database and Nginx reverse proxy. I
believe this is a fairly typical example of a real-world
use-case.

Typically, one would probably separate out the database and
reverse proxy into a different container. This would be very simple to
do here too, but I'll leave that as an exercise to the reader.

Also, *side-note*: I'll worry too much about our example services
security, so I'll not setup a firewall, or restrict a services listen
addresses. I'll also not protect any of the secrets or passwords,
something one would typically do. Check out [this
page](https://freckles.io/doc/security) for pointers on how to do this
properly with *freckles*.

First, let's create a user and group for our application:

``` yaml
- user-exists:
    name: webapp
    group: webapp
```

Then we'll need a PostgreSQL service on our host, along with a
database our webapp can access. With *freckles*, we can provision one
using the
[``postgresql-service``](https://freckles.io/frecklets/default/service/postgresql-service) and
[``postgresql-database-exists``](https://freckles.io/frecklets/default/service/postgresql-database-exists)
frecklets:

``` yaml
- postgresql-service:
    listen_addresses:
      - '*'
    pg_hba:
      - method: md5
        address: 0.0.0.0/0
- postgresql-database-exists:
    db_name: webapp
    db_user: webapp
    db_user_password: webapp_password
```

Now we need the source code for our webapp:

``` yaml
- git-repo-synced:
    repo: "https://github.com/gothinkster/flask-realworld-example-app.git"
    dest: "/opt/realworld-app/"
    owner: webapp
    group: webapp
```

Then, we'll want to create a virtualenv for our Python flask app, and
for that we can use the
[``python-virtualenv``](https://freckles.io/frecklets/default/languages/python-virtualenv)
*freckelt*:

``` yaml
- python-virtualenv:
    venv_name: lxd_example
    python_type: pyenv
    python_version: 3.7.3
    user: webapp
    group: webapp
    system_dependencies:
      - postgresql-server-dev-all
    python_packages:
      - Werkzeug
      - SQLAlchemy==1.1.9
      - Flask_Caching
      - Flask_SQLAlchemy==2.2
      - click
      - marshmallow
      - Flask_Bcrypt
      - flask_apispec
      - Flask
      - PyJWT
      - Flask-JWT-Extended
      - unicode_slugify
      - psycopg2
      - Flask-Migrate
      - gunicorn
      - Flask-Cors
```

As a next step, we need to initialize our database, with the
appropriate environment variables set so our flask app knows how to
connect to our db server:

``` yaml
- python-virtualenv-execute-shell-commands:
    commands:
      - "flask db init"
      - "flask db migrate"
      - "flask db upgrade"
    virtualenv_path: "/home/webapp/.pyenv/versions/lxd_example"
    chdir: "/opt/realworld-app"
    environment:
      FLASK_APP: "/opt/realworld-app/autoapp.py"
      FLASK_DEBUG: 0
      DATABASE_URL: "postgresql+pysycopg2://webapp:webapp_password@localhost/webapp"
```
