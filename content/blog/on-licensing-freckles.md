+++
title = "On licensing freckles"
date = "2019-06-17T18:00:00+02:00"
tags = ["freckles", "licensing"]
categories = ["general"]
banner = "img/banners/data-centric-environment-management.jpg"
+++

Ok, I guess I have to write this. Why is *freckles* licensed using [the Parity Public license](https://licensezero.com/licenses/parity), a license that most people haven't heard of (yet), that is not approved by FSF/OSI, and that is also very *very* copy-left, compared to the usual suspects?

There are a few reasons, and it took me quite a while to arrive at the conclusions I eventually arrived at. I'm pretty confident anything I say here has been said before, and most likely much more eloquent and exhaustive. Probably by [Kyle Mitchell](https://kemitchell.com/), whose writings [here](https://writing.kemitchell.com/) and [here](https://blog.licensezero.com/) I can't recommend enough.

**Sidenote**: Parity allows you to run software licensed with it in combination with other open-source software, as much as you want. If you want to use it in combination with non-open software, you'll have to purchase an alternative license for the code in question. Read the license text, it's very short!

*Oh, one thing, before I start*: I usually avoid calling *freckles* 'open-source'. Not because I think it is not 'open-source software', but because the OSI claims to own the term, and any license that is not approved by it should not be called 'open-source'. A few (loud-ish) people on the usual opinion-haunts agree. So, that's why I just don't call *freckles* open source software: because I'm not interested in that particular argument. In this blog post I will do so though. Because in this context here it's just easier, and to me, software licensed using the Parity Public license counts as open-source software, and is not too far away from what Richard Stallman wanted, back when. In a lot of ways it's probably closer than to what the OSI stands for today. But, as I said: not having that argument. This is really about my (personal) decision to use a very strong copy-left license.

*Oh, and another thing*: the main reason I wrote *freckles* was because I very strongly felt that something like it should exists, and I could not understand that it didn't yet. Because it seems really obvious to me, to a point where I can't even explain why. I've actually waited a few years before I (reluctantly) started writing it. It's not the most interesting thing to write, really. Anyway: the parts of me that have nothing to do with my programming-related obsessions obviously realize that the fact that nobody ever bothered writing it, in addition to the blank looks I usually get when I tell people about it, indicate that this is probably really nothing nobody ever wants to use. Which makes it a bit embarrassing when I talk about actually charging people money for it. For the purpose of this article, just assume I'm talking about something people would be at least somewhat inclined to pay for, ok?

With all that being said:

# Reasons!!!

## Idealism

I would like to live in a world where all software is open-source. Boring people will say that is not realistic, but boring people would say that, wouldn't they? I reluctantly agree that this will not transpire in the next short to very long future, but hey, that doesn't mean that we who think that this should be the case shouldn't do their small parts. *Parity* gives open source software a leg up, because it can be used without any problems from other open-source software, but needs to be purchased when people want to use it with code they don't want to share.

## Money

This probably sounds like a humble-brag, but I'm not really good at half-assing things. I don't see that as a strength all of the time, but sometimes it is. Anyway, I wasn't really happy with my first 'proof-of-concept' for *freckles*, even if it kind of worked. I really wanted to properly understand the thing I was trying to fix, and make it as *simple* (not [*easy*](https://www.infoq.com/presentations/Simple-Made-Easy/)) as I could. This took me about 3 or 4 re-writes. Now I'm happy (enough) in this regard. But I also spent a shi*p*-load of time on it. And I wouldn't have done it if I didn't think there was at least a *very* small chance that it could pay off in some other way than me being satisfied about having understood something. Motivations, and how the prospect of reward driving society, and all that.

If I had chosen any of the 'normal' open source licenses (even AGPL), there wouldn't be much of an incentive for people to pay for *freckles*. The triggers that enforce sharing your code just don't apply. I could have, of course, chose one of the other open-source business models, not dual-licensing:

### Open-source business models, and what I don't like about them

#### **Donations**

This is not the relationship I want to have with my users. I don't think any party in such a relationship should feel 'thankful' for something the other party did. I don't want to feel thankful for users having donated, even though they didn't need to, and users shouldn't need to feel thankful because I created something and gave it out for free. I like the sort of relationships that result from market-driven interactions much better: I take a risk, and spend time and money creating something I think can be of value for you. You wait until it is clear that thing is of value for you, and you pay for that value (plus a bit extra for the risk I took). My free decision to take that risk, your free decision to pay for a product after you compared it to other options. Nobody has to be thankful, and we can all be adults about it. No (perceived, or real) power imbalance.

#### **Open core**

I don't like the overhead this creates, in terms of having to split up code more or less arbitrarily. Driven by business decisions, not what would make sense technically. A code-base likt that is much more cumbersome to deal with, especially in regards to having to setup everything twice (CI pipelines, release channels, ...). But more importantly, it's going to be really difficult to align my incentives with my users interests.

Also, 'idealism': everybody can use everything, if you make you own software freely available, you might have to have a slight advantage over your proprietary competitor by not having to pay.

#### **Software/platform/whatever as a service**

This would have been possible for *freckles*, and probably would have been a bit easier to implement too (but harder to run/maintain in the long run). But it would have taken away a lot of value from the user. The way it is now, *freckles* is super-flexible, and can be used in a lot of circumstances, without much hassle at all: embed the binary in your project, use the Python library and Python code, write a small REST-wrapper around it and call it remotely, host the binary yourself, use my hosted version, etc. If it was a hosted service, users would have to trust me with their secrets, and data in general. I might still decide to provide such a hosted service at some point, but it'd be in addition to all the other options users can pick from.

Also, I really dislike how there are so many startups creating SaaSes for things that *should* just be libraries, if you look at it from a technical perspective. I see how MRR is nice, but really, this is getting pretty silly.

#### **Corporate sponsorship**

I could live with that, I guess. Depending on the company, and terms. Still, I like the idea of a software-cornershop with a handful (enough to sustain) users better.

## Tit for tat

I think it's only fair: if you don't want to give me your software, why should I let you use mine? There are not a lot of good moral arguments to be made for why the freedom of people who don't want to protect their users freedom should be protected. There is an argument to be made by open-source developers who want to protect *their* users freedom (even if those users don't want to protect *their* users freedom in turn). If that's you, fine, I'll grant you that. But I might want you to show me what and how much you created and invested, first.

For now, that's all I can think of. Will add to this over time.
