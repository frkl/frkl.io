+++
title = "freckles & ssh"
date = "2019-08-01T00:00:00+02:00"
tags = ["freckles"]
categories = ["general", "ssh", "beginners"]
draft = true
+++

This is the second (and final) part of the blog post series about how
to -- and esp. *why* -- use *ssh*. The first part dealt with how to
create an ssh key, and add the public part of it to remote services in
order to be able to authenticate against them. This part is shorter,
and will outline how this is done in combination with *freckles*.

The main purpose of *freckles* is to setup and provision machines, be
they physical, virtual, or containerized. Except for the case when you
use it to setup the machine you are logged in already (locally),
you'll have to have a way to connect to a remote machine. And that --
almost always -- involves *ssh*.

It's possible to use password authentication with *freckles*, using
the ``'--ask-login-pass``' command-line flag. In most cases that is
not desirable though, because that means that either your workflow
can't be scripted (because of a manual password input), or is somewhat
insecure (because there is no really save way to use passwords in
shell scripts without exposing them in some way).

So, better to use ssh keys! We'll be using *freckles* to create an ssh
key (and to run ssh-agent). Usually you'd do that manually, as it's a
one time task and automating it doesn't make all to much sense. But
there are cases where automating this actually makes sense (testing
the setup of complete infrastructures incl. control host, etc.). Plus,
I've already shown how to do that manually, anyway...

To create an ssh key, we'll be using the
``[ssh-key-exists](https://freckles.io/frecklets/default/system/ssh-key-exists)``
frecklet:

``` console
frecklecute ssh-key-exists --password ::ask::
```

This will create a password-protected 4096 bit/ed25519-type ssh key in
``$HOME/.ssh/id_rsa``. *freckles* will *ask* for the private key password
(because we 'ask' it to, with '``::ask::``'), as it's not a good idea to
provide passwords in a command-line invocation of an application (as
it'll be stored in the shell history, and visible in the process
tree).

Sometimes we need a host to have a passwordless ssh key (for example
to be able to checkout a private git repository in a batch job). In
this case we can just omit the ``--password`` argument (just note that
this will be inherently more insecure than having a password-protected
key):

``` console
frecklecute ssh-key-exists
```

Now that we have an ssh-key, we should use it to connect to
things. Let's assume we have
